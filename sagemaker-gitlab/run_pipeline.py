import json
import sys
from get_pipeline import get_pipeline
import argparse


parser = argparse.ArgumentParser(
        "Creates or updates and runs the pipeline for the pipeline script."
    )


parser.add_argument(
	"--role-arn", 
	dest = "role_arn", 
	type = str, 
	help = "Role arn for the pipeline execution Role"
	)

parser.add_argument(
        "-description",
        "--description",
        dest="description",
        type=str,
        default= "Pipeline for train a model that explains how to measure taxi trips duration on New york" ,
        help="Description of the pipeline.",
    )

args = parser.parse_args()


def main():
	try:
		pipeline = get_pipeline(region = "us-west-2")
		parsed = json.loads(pipeline.definition())

		sys.executable("export PIPELINE_ARN={pipeline.arn}")
		sys.executable("export PIPELINE_NAME={pipeline.name}")

		print(json.dumps(parsed, indent = 2, sort_keys = True))
		upsert_response = pipeline.upsert(
			role_arn=args.role_arn, description=args.description)
		print(upsert_response)
		execution = pipeline.start()
		print(f"ExecutionArn: {execution.arn}")
		execution.wait()
		steps = execution.list_steps()
		register_model_step = next(s for s in steps if s['StepName'] == 'RegisterModel')
		model_package_arn = register_model_step['Metadata']['RegisterModel']['Arn']
		
		with open('pipeline_definition.json', 'w') as f:
			json.dump(parsed, f)

	except Exception as e: 
		print(f"Exception: {e}")
		sys.exit(1)
	
	return model_package_arn


if __name__ == "__main__": 
	main()