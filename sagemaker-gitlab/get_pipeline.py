import os 


import boto3
import sagemaker
from sagemaker import xgboost
import sagemaker.session



from sagemaker.inputs import TrainingInput
from sagemaker.model_metrics import (
    MetricsSource,
    ModelMetrics,
)
from sagemaker.processing import (
    ProcessingInput,
    ProcessingOutput
)
from sagemaker.sklearn.processing import SKLearnProcessor
from sagemaker.workflow.conditions import ConditionGreaterThanOrEqualTo
from sagemaker.workflow.condition_step import (
    ConditionStep,
    JsonGet,
)
from sagemaker.workflow.parameters import (
    ParameterInteger,
    ParameterString,
    ParameterFloat
)
from sagemaker.workflow.pipeline import Pipeline
from sagemaker.workflow.steps import (
    ProcessingStep,
    TrainingStep,
)
from sagemaker.workflow.step_collections import RegisterModel

from sagemaker.xgboost import XGBoost


BASE_DIR = os.path.dirname(os.path.realpath(__file__))

print(BASE_DIR)


def get_sagemaker_client(region):
     """Gets the sagemaker client.

        Args:
            region: the aws region to start the session
            default_bucket: the bucket to use for storing the artifacts

        Returns:
            `sagemaker.session.Session instance
        """
     boto_session = boto3.Session(region_name=region)
     sagemaker_client = boto_session.client("sagemaker")
     return sagemaker_client

def get_session(region, default_bucket):
    """Gets the sagemaker session based on the region.

    Args:
        region: the aws region to start the session
        default_bucket: the bucket to use for storing the artifacts

    Returns:
        `sagemaker.session.Session instance
    """

    boto_session = boto3.Session(region_name=region)

    sagemaker_client = boto_session.client("sagemaker")
    runtime_client = boto_session.client("sagemaker-runtime")
    return sagemaker.session.Session(
        boto_session=boto_session,
        sagemaker_client=sagemaker_client,
        sagemaker_runtime_client=runtime_client,
        default_bucket=default_bucket,
    )


def get_pipeline(
    region, 
    role = "sagemakerAuto-v1-dev-AutomatedRole-NJNA4SYCYCKN", 
    default_bucket = "datalake-model-artifacts", 
    model_package_group_name = "TaxiTripPackage", 
    pipeline_name = "TaxiTripPipeline", 
    base_job_prefix = "taxi-trip-model"):

    sagemaker_session = get_session(region, default_bucket)
    if role is None: 
        role = sagemaker.session.get_execution_role(sagemaker_session)
    
    
    # parameters for pipeline execution
    processing_instance_count = ParameterInteger(name="ProcessingInstanceCount", default_value=1)
    processing_instance_type = ParameterString(
        name="ProcessingInstanceType", default_value="ml.m5.xlarge"
    )
    training_instance_type = ParameterString(
        name="TrainingInstanceType", default_value="ml.m5.xlarge"
    )
    input_data = ParameterString(
        name="InputDataUrl",
        default_value=f"s3://datalake-model-artifacts/taxi-trip/train.csv",
    )

    model_approval_status = ParameterString(
        name='ModelApprovalStatus', 
        default_value='PendingManualApproval'
        )

    minimum_precision = ParameterFloat(
        name='MinimumRMSE', 
        default_value=0.40
        )

    # Processing Step
    train_data_file = 'train_data.csv'
    test_data_file = 'test_data.csv'
    validation_data_file = "validation_data.csv"
    encoder_file = 'encoder.pkl'

    sklearn_processor = SKLearnProcessor(
        framework_version = "0.23-1", 
        instance_type = processing_instance_type, 
        instance_count = processing_instance_count, 
        base_job_name = f"{base_job_prefix}/sklearn-taxitrip-preprocess", 
        sagemaker_session=sagemaker_session, 
        role = role
    )
    
    step_preprocess = ProcessingStep(
        name = "PreprocessTaxiTrip", 
        processor = sklearn_processor, 
        inputs = [
            ProcessingInput(source = f"s3://{default_bucket}/taxi-trip/train.csv", destination = "/opt/ml/processing/input")
        ], 
        outputs = [
            
            ProcessingOutput(output_name="train_data", 
                source = "opt/ml/processing/output/train_data",
                destination = f's3://{default_bucket}/preprocess/train_data'), 
            ProcessingOutput(output_name="test_data", 
                source = "opt/ml/processing/output/test_data",
                destination = f's3://{default_bucket}/preprocess/test_data'), 
            ProcessingOutput(output_name="validation_data", 
                source = "opt/ml/processing/output/validation_data",
                destination = f's3://{default_bucket}/preprocess/validation_data'),
            ProcessingOutput(output_name='encoder',
                    source=f'/opt/ml/processing/output/encoder',
                    destination=f's3://{default_bucket}/preprocess/encoder')
        ], 
        code = os.path.join(BASE_DIR, "scripts/preprocess.py"), 
        job_arguments = [
            '--data-file', 'train.csv',
            '--train-data-file', train_data_file,
            '--test-data-file', test_data_file,
            '--validation-data-file', validation_data_file,
            '--encoder-file', encoder_file
        ]
    )

    # training step for generating model artifacts
    model_path = f"s3://{default_bucket}/{base_job_prefix}/xgboost/model"

    entry_point = "training.py "
    source_dir = "scripts/"

    output_path = 's3://{0}/{1}/output/'.format(default_bucket, base_job_prefix)
    code_location = 's3://{0}/{1}/code'.format(default_bucket, base_job_prefix)

    hyperparameters = {
        'booster': 'gbtree',
        'objective': 'reg:linear',
        'max_depth': 14,
        'subsample': 0.8,
        'colsample_bytree':  0.7,
        'colsample_bylevel':  0.7,
        'eval_metric': "rmse", 
        "lambda": 0.7, 
        "num_round": 50
    }

    xgb_train = XGBoost(
        base_job_name="taxi-trip-model",
        entry_point=entry_point,
        source_dir=source_dir,
        output_path=output_path,
        code_location=code_location,
        hyperparameters=hyperparameters,
        instance_type="ml.m5.xlarge",
        instance_count=1,
        framework_version="0.90-2",
        py_version="py3",
        role=role
    )

    step_train = TrainingStep(
        name="TrainTaxiTripEstimation",
        estimator=xgb_train,
        inputs={
            "train": TrainingInput(
                s3_data=step_preprocess.properties.ProcessingOutputConfig.Outputs[
                    "train"
                ].S3Output.S3Uri,
                content_type="text/csv",
            ),
            "validation": TrainingInput(
                s3_data=step_preprocess.properties.ProcessingOutputConfig.Outputs[
                    "validation"
                ].S3Output.S3Uri,
                content_type="text/csv",
            ),
        },
    )

    # Evaluation Step
    entry_point = "evaluate.py"
    source_dir = "scripts/"


    evaluate_processor = SKLearnProcessor(
        framework_version = "0.23-1", 
        instance_type = processing_instance_type, 
        instance_count = processing_instance_count, 
        base_job_name = f"{base_job_prefix}-evaluate", 
        sagemaker_session=sagemaker_session, 
        role = role
    )

    step_evaluate = ProcessingStep(
        name = "PreprocessTaxiTrip", 
        processor = evaluate_processor, 
        inputs = [
            ProcessingInput(source = f"s3://{default_bucket}/taxi-trip/train.csv", destination = "/opt/ml/processing/input")
        ], 
        outputs = [
            ProcessingOutput(output_name="train_data", 
                source = "opt/ml/processing/output/train_data",
                destination = f's3://{default_bucket}/preprocess/train_data'), 
            ProcessingOutput(output_name="test_data", 
                source = "opt/ml/processing/output/test_data",
                destination = f's3://{default_bucket}/preprocess/test_data'), 
            ProcessingOutput(output_name="validation_data", 
                source = "opt/ml/processing/output/validation_data",
                destination = f's3://{default_bucket}/preprocess/validation_data'),
        ], 
        code = os.path.join(BASE_DIR, "scripts/preprocess.py"), 
        job_arguments = [
            '--data-file', 'train.csv',
            '--train-data-file', train_data_file,
            '--test-data-file', test_data_file,
            '--validation-data-file', validation_data_file,
            '--encoder-file', encoder_file
        ]
    )

    # Register Step

    model_metrics = ModelMetrics(
        model_statistics= MetricsSource(
            s3_uri = "{}/metrics.json".format(
                step_evaluate.arguments["ProcessingOutputConfig"]["Outputs"][0]["S3Output"]["S3Uri"]),
                content_type="application/json"
        )
    )

    register_step = RegisterModel(
        name = "Model Registry", 
        estimator = xgb_train, 
        model_data = step_train.properties.ModelArtifacts.S3ModelArtifacts, 
        content_types=["text/csv"],
        response_types=["text/csv"],
        inference_instances=["ml.t2.medium", "ml.m5.large"],
        transform_instances=["ml.m5.large"],
        model_package_group_name=model_package_group_name,
        approval_status=model_approval_status,
        description=f'XgBoost model',
        model_metrics=model_metrics,
        entry_point = "scripts/training.py"
    )
    condition = ConditionGreaterThanOrEqualTo(
        left = JsonGet(
            step = step_evaluate,
            property_file = "metrics.json",
            json_path = f'mse.value'),
        right = minimum_precision)
        
    condition_step = ConditionStep(
        name=f"Model Mean Squared Error",
        conditions=[condition],
        if_steps=[register_step],
        else_steps=[])

    # pipeline instance
    pipeline = Pipeline(
        name=pipeline_name,
        parameters=[
            processing_instance_type,
            processing_instance_count,
            training_instance_type,
            model_approval_status,
            input_data,
        ],
        steps=[
            step_preprocess, 
            step_train, 
            step_evaluate, 
            condition_step],
        sagemaker_session=sagemaker_session,
    )
    return pipeline

if __name__ == "__main__": 
    get_pipeline(region = "us-west-2").definition()