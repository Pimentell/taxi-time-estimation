import boto3
import argparse
import sys
import os 


def main(): 
    response ={}
    parser = argparse.ArgumentParser(
        "Creates or updates and runs the pipeline for the pipeline script."
    )

    parser.add_argument(
        "-n",
        "--model_name",
        dest="model_name",
        type=str,
        default= "taxi-trip-model" ,
        help="Model Name for create the rule"
    )

    parser.add_argument(
        "-s", 
        "--scheduled_expression", 
        dest = "scheduled_expression", 
        default = "cron(0 0 1/15 * ? *)"
    )

    parser.add_argument( 
        "--state", 
        dest = "state", 
        default = "ENABLED"
    )

    parser.add_argument(
        "-description",
        "--description",
        dest="description",
        type=str,
        default= "Add Event Rule" 
    )
    parser.add_argument(
        "--role-arn",
        dest="role_arn",
        type=str,
        default= "Execution Role" 
    )
    parser.add_argument(
        "--pipeline_name",
        dest="pipeline_name",
        type=str,
        default= "pipeline_name" 
    )
    parser.add_argument(
        "--pipeline_arn",
        dest="pipeline_arn",
        type=str,
        default= "pipeline arn" 
    )

    parser.add_argument(
        "--bucket",
        dest="bucket",
        type=str,
        default= "bucket" 
    )
    parser.add_argument(
        "--dataset-prefix",
        dest="dataset_prefix",
        type=str,
        default= "Data Set Prefix" 
    )
    
    args = parser.parse_args()

    events_client = boto3.client("events")

    put_rule_response = events_client.put_rule(
        Name = parser.model_name + "ExecutioRule", 
        ScheduleExpression = args.scheduled_expression, 
        State = args.state, 
        Description = args.description, 
        RoleArn= args.role_arn
    )
    sys.executable(f"export RULE_ARN={put_rule_response['RuleArn']}")

    response["RuleArn"] = put_rule_response['RuleArn']
    
    add_rule_response = events_client.put_targets(
        Rule= args.model_name + "ExecutioRule",
        Targets=[{
            'Id': args.pipeline_name,
            'Arn': args.pipeline_arn,
            'RoleArn': os.environ["SageMakerRole"],
            'SageMakerPipelineParameters': {
                'PipelineParameterList': [
                    {
                        'Name': 'DatasetPath',
                        'Value': f's3://{args.bucket}/{args.dataset_prefix}'
                    },
                    {
                        'Name': 'ModelApprovalStatus',
                        'Value': 'PendingManualApproval'
                    },
                    {
                        'Name': 'MinimumRMSE',
                        'Value': '0.40'
                    },
                ]
            },
        }]
    )
    try:
         Failed = add_rule_response["FailedEntryCount"]
         response["AddToRule"] = add_rule_response
    except: 
        response["AddToRule"] = "Exit"

    return response

if __name__ == "__main__": 
    response = main()
    print(response)