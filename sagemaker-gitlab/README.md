
![Imagen](images/sagemakerGitlabML.jpg)

This example shows how Gitlab runners can be used to create and run the sagemaker inference pipeline. However, it leaves out the automation of the enpoint and lambda creation. On the other hand, these manually configurable parts can be carried out through the administration console.