import os 
import argparse
import sys
import logging
import warnings
import calendar

import pandas as pd 
import numpy as np
import pickle
import datetime
import math


from sklearn.preprocessing import LabelEncoder
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split


warnings.filterwarnings("ignore")


# Logger SetUp
# -------------------------------------------------------------------------------
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logFormatter = logging.Formatter(
    "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(logFormatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)


today = str(datetime.datetime.now())[:13].replace(" ", "-").replace("-", "_")

input_path = "/opt/ml/processing/input"
train_path = "/opt/ml/processing/train"
validation_path = "/opt/ml/processing/val"

model_columns = [
    "vendor_id", 
    "passenger_count", 
    "pickup_latitude_round3",            
    "pickup_longitude_round3",            
    "dropoff_latitude_round3",          
    "dropoff_longitude_round3",
    "distance", 
    "pickup_neighbourhood", 
    "dropoff_neighbourhood", 
    "store_and_fwd_flag", 
    "pickup_day", 
    "pickup_hour", 
    "pickup_day_of_week"
]

target_column = [
    "trip_duration"	]




def haversine(coord1, coord2):
    R = 6372800
    lat1, lon1 = coord1
    lat2, lon2 = coord2
    
    phi1, phi2 = math.radians(lat1), math.radians(lat2) 
    dphi       = math.radians(lat2 - lat1)
    dlambda    = math.radians(lon2 - lon1)
    
    a = math.sin(dphi/2)**2 + \
        math.cos(phi1)*math.cos(phi2)*math.sin(dlambda/2)**2
    
    return 2*R*math.atan2(math.sqrt(a), math.sqrt(1 - a))


def get_distance(row): 
    
    pick_lat = row["pickup_latitude"]
    pick_lon  = row["pickup_longitude"]

    drop_lat = row["dropoff_latitude"]
    drop_lon =  row["dropoff_longitude"]

    coordinates_pick = (pick_lat,pick_lon)
    coordinates_drop = (drop_lat, drop_lon)

    distance = haversine(coordinates_pick, coordinates_drop)

    return distance

def encodeDays(day_of_week):
    day_dict = {'Sunday':0, 'Monday':1, 'Tuesday':2, 'Wednesday':3, 'Thursday':4,
                'Friday':5, 'Saturday':6}
    return day_dict[day_of_week]


def to_pkl(data, file):
    with open(file, 'wb') as f:
        pickle.dump(data, f)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-test-split-ratio', type=float, default=0.3)
    args, _ = parser.parse_known_args()
    
    
    try:
        logging.info("Extracting data")
        input_data_path = os.path.join(input_path, 'raw_data.csv')
        data = pd.read_csv(input_data_path)
        print(data.shape)
    except Exception as e: 
        logger.exception(f"Exception -- Can not read data due to {e}")
        sys.exit(2)
    
    try: 
        logging.info("Starts Preprocessing data")
        data['pickup_datetime'] = pd.to_datetime(data['pickup_datetime'], format = '%Y-%m-%d %H:%M:%S')
        data['pickup_date'] = data['pickup_datetime'].dt.date
        data['pickup_day'] = data['pickup_datetime'].apply(lambda x : x.day)
        data['pickup_hour'] = data['pickup_datetime'].apply(lambda x : x.hour)
        data['pickup_day_of_week'] = data['pickup_datetime'].apply(lambda x : calendar.day_name[x.weekday()])
        data['pickup_latitude_round3'] = data['pickup_latitude'].apply(lambda x : round(x, 3))
        data['pickup_longitude_round3'] = data['pickup_longitude'].apply(lambda x : round(x, 3))
        data['dropoff_latitude_round3'] = data['dropoff_latitude'].apply(lambda x : round(x, 3))
        data['dropoff_longitude_round3'] = data['pickup_longitude'].apply(lambda x : round(x, 3))

        le = LabelEncoder()
        le.fit(data['store_and_fwd_flag'])
        data['store_and_fwd_flag'] = le.transform(data['store_and_fwd_flag'])

        data['pickup_day_of_week'] = data['pickup_day_of_week'].apply(lambda x:encodeDays(x))
        data["distance"] = data.apply(lambda row: get_distance(row), axis = 1)
    
        coords = np.vstack((data[['pickup_latitude', 'pickup_longitude']].values,
                           data[['dropoff_latitude', 'dropoff_longitude']].values,
                           data[['pickup_latitude', 'pickup_longitude']].values,
                           data[['dropoff_latitude', 'dropoff_longitude']].values))

        kmeans = KMeans(n_clusters = 10, random_state = 0).fit(coords)

        data.loc[:, 'pickup_neighbourhood'] = kmeans.predict(data[['pickup_latitude', 
                                                                    'dropoff_longitude']])
        data.loc[:, 'dropoff_neighbourhood'] = kmeans.predict(data[['dropoff_latitude',
                                                                'pickup_longitude']])

        X = data[model_columns]
        y = data[target_column]
        
        split_ratio = args.train_test_split_ratio
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=split_ratio, 
                                                      random_state=0)
        
        train_features_output_path = os.path.join(train_path, 'train_features.csv')
        train_labels_output_path = os.path.join(train_path, 'train_labels.csv')
    
        val_features_output_path = os.path.join(validation_path, 'val_features.csv')
        val_labels_output_path = os.path.join(validation_path, 'val_labels.csv')
    
        logging.info("Saving Train and validation data")
        pd.DataFrame(X_train).to_csv(train_features_output_path, header=False, index=False)
        pd.DataFrame(X_val).to_csv(val_features_output_path, header=False, index=False)
        pd.DataFrame(y_train).to_csv(train_labels_output_path, header=False, index=False)
        pd.DataFrame(y_val).to_csv(val_labels_output_path, header=False, index=False)
        
        
        # TO DO  Add model as pipeline for add it to Sagemaker Pipeline. 
        
    except Exception as e: 
        logger.exception(f"Exception -- Can not read data due to {e}")
        sys.exit(2)
    return data

if __name__ == "__main__": 
    main()