import pickle
import os
import json
import tarfile
import pandas as pd
import numpy as np
import os 
import argparse
import sys
import logging
import warnings
from sklearn.metrics import mean_squared_error, mean_absolute_error

warnings.filterwarnings("ignore")


# Logger SetUp
# -------------------------------------------------------------------------------
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logFormatter = logging.Formatter(
    "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(logFormatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

from sklearn.metrics import precision_recall_curve

def load_model(file, model_file='model.pkl'):
    if file.endswith('tar.gz'):
        with tarfile.open(file, 'r:gz') as tar:
            for name in tar.getnames():
                if name == model_file:
                    f = tar.extractfile(name)
                    return pickle.load(f)
            return None
    elif file.endswith('pkl'):
        with open(file, 'rb') as f:
            return pickle.load(f)
    else:
        return None


def main(): 
    script_name = os.path.basename(__file__)
    logging.info("Iniciando la evaluación del modelo")

    parser = argparse.ArgumentParser()
    parser.add_argument('--algo', type=str, required=True)
    parser.add_argument('--min-score', type=float, required=True)    
    parser.add_argument('--test-data-file', type=str, required=True)
    parser.add_argument('--test-target-file', type=str, required=True)
    parser.add_argument('--thresholds-file', type=str, required=True)   
    parser.add_argument('--metrics-report-file', type=str, required=True)   

    args, _ = parser.parse_known_args()    
    
    print(f'INFO: {script_name}: Parámetros recibidos: {args}')
    
    input_path = '/opt/ml/processing/input'
    output_path = '/opt/ml/processing/output'


    # Cargar datasets
    test_target_path = os.path.join(input_path, 'target', args.test_target_file)     
    test_target = pd.read_csv(test_target_path)
    
    test_data_path = os.path.join(input_path, 'data', args.test_data_file)     
    test_data = pd.read_csv(test_data_path)

    model_path = os.path.join(input_path, args.algo, 'model.tar.gz') 
    logging.info("Loading model")

    clf = load_model(model_path)
    predictions = clf.predict(test_data)
    mse = mean_squared_error(test_target, predictions)
    mae = mean_absolute_error(test_target, predictions)

    metrics_repo = {
            'mse': {'value': mse},
            'mae': {'value': mae,}
    }
    metrics_repo.to_csv(os.path.join(output_path, args.thresholds_file), index=False) 


if __name__ == "__main__": 
    main()