# Examples for Infra to use with model Taxi Trip Estimation

Víctor Eduardo Pimentel Avilés


1. Serverless


![Imagen](serverless/images/ServerlessMLTaxi.jpeg)


It allows to use in a much more flexible way the training capabilities in Glue Job and Lambda. Using as the main instrument for the orchestration the creation of pipelines through StepFunctions.

One of the main advantages is the permissibility that it gives the ML engineer to be able to modify any of the characteristics within the processing or even the training steps As well as adding different monitoring components, alarms and even automations


2. Pure Sagemaker Notebooks

![Imagen](sagemaker-notebooks/images/sagemakerMLTaxi.jpg)


In this example we use a Sagemaker notebook to create the entire infrastructure and deployment of it. The versioning is found only through the different versions of the Notebook. 


3. Sagemaker - Gitlab

![Imagen](sagemaker-gitlab/images/sagemakerGitlabML.jpg)

This example shows how Gitlab runners can be used to create and run the sagemaker inference pipeline. However, it leaves out the automation of the enpoint and lambda creation. On the other hand, these manually configurable parts can be carried out through the administration console.