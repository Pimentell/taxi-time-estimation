import argparse
import json
import os
import random
import pandas as pd
import glob
import pickle as pkl
import xgboost as xgb
import numpy as np




def main(): 
    
    # Parsing Arguments
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--eta", type=float, default=0.05)
    parser.add_argument("--gamma", type=int, default=4)
    parser.add_argument("--min_child_weight", type=int, default=6)
    parser.add_argument("--silent", type=int, default=1)
    parser.add_argument("--objective", type=str, default="reg:lineal")
    parser.add_argument("--eval_metric", type=str, default="rmse")
    parser.add_argument("--num_round", type=int, default=10)
    parser.add_argument('--train', type=str, default=os.environ.get('SM_CHANNEL_TRAIN'))
    parser.add_argument('--validation', type=str, default=os.environ.get('SM_CHANNEL_VALIDATION'))
    parser.add_argument('--booster', type=str, default = "gbtree")
    parser.add_argument('--max_depth', type=int, default = 14)
    parser.add_argument('--subsample', type=float, default = 0.8)
    parser.add_argument('--colsample_bytree', type=float, default = 0.7)
    parser.add_argument('--colsample_bylevel', type=float, default = 0.7)
    parser.add_argument('--lambda', type=float, default=0.7)
    
    args = parser.parse_args()
    train_files_path, validation_files_path = args.train, args.validation
    train_features_path = os.path.join(args.train, 'train_features.csv')
    train_labels_path = os.path.join(args.train, 'train_labels.csv')
    val_features_path = os.path.join(args.validation, 'val_features.csv')
    val_labels_path = os.path.join(args.validation, 'val_labels.csv')
    
    
    print('Loading training dataframes...')
    df_train_features = pd.read_csv(train_features_path, header=None)
    df_train_labels = pd.read_csv(train_labels_path, header=None)
    
    print('Loading validation dataframes...')
    df_val_features = pd.read_csv(val_features_path, header=None)
    df_val_labels = pd.read_csv(val_labels_path, header=None)
    
    X = df_train_features.values
    y = df_train_labels.values.reshape(-1)
    
    val_X = df_val_features.values
    val_y = df_val_labels.values.reshape(-1)
    
    print('Train features shape: {}'.format(X.shape))
    print('Train labels shape: {}'.format(y.shape))
    print('Validation features shape: {}'.format(val_X.shape))
    print('Validation labels shape: {}'.format(val_y.shape))
    
    ## train model
    dtrain = xgb.DMatrix(X, np.log(y +1))
    dval = xgb.DMatrix(val_X, np.log(val_y+1))
    
    watchlist = [(dtrain, "train"), (dval, "validation")]
    
    
    params = {
        "booster": args.booster, 
        "objective": args.objective,
        "max_depth": args.max_depth,
        "subsample": args.subsample, 
        "colsample_bytree": args.colsample_bytree, 
        "colsample_bylevel": args.colsample_bylevel,
        "silent": args.silent,
        "eval_metric": args.eval_metric
    }

    bst = xgb.train(
        params=params,
        dtrain=dtrain,
        evals=watchlist,
        num_boost_round=args.num_round)
    
    model_dir = os.environ.get('SM_MODEL_DIR')
    pkl.dump(bst, open(model_dir + '/model.bin', 'wb'))
    
    
if __name__ == "__main__": 
    main()