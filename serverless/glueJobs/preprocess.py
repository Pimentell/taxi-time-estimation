import pandas as pd 
import numpy as np
import geopy
import calendar
from sklearn.preprocessing import LabelEncoder
import argparse
import os 
from geopy.distance import geodesic
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
import sys
import logging
from awsglue.utils import getResolvedOptions
import warnings
import pickle
from utils import csv_file
import datetime
today = str(datetime.datetime.now())[:13].replace(" ", "-").replace("-", "_")

warnings.filterwarnings("ignore")


# Logger SetUp
# -------------------------------------------------------------------------------
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logFormatter = logging.Formatter(
    "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(logFormatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)


BUCKET = getResolvedOptions(sys.argv, ["bucket"])
BUCKET = BUCKET["bucket"]

INPUT_PREFIX = getResolvedOptions(sys.argv, ["input-prefix"])
INPUT_PREFIX = INPUT_PREFIX["input-prefix"]

OUTPUT_PREFIX = getResolvedOptions(sys.argv, ["output-prefix"])
OUTPUT_PREFIX = OUTPUT_PREFIX["output-prefix"]



data_path = f"s3://{BUCKET}/{INPUT_PREFIX}"

def get_distance(row): 
    
    pick_lat = row["pickup_latitude"]
    pick_lon  = row["pickup_longitude"]

    drop_lat = row["dropoff_latitude"]
    drop_lon =  row["dropoff_longitude"]

    coordinates_pick = (pick_lat,pick_lon)
    coordinates_drop = (drop_lat, drop_lon)

    distance = geodesic(coordinates_pick, coordinates_drop)

    return distance.km

def encodeDays(day_of_week):
    day_dict = {'Sunday':0, 'Monday':1, 'Tuesday':2, 'Wednesday':3, 'Thursday':4,
                'Friday':5, 'Saturday':6}
    return day_dict[day_of_week]


def to_pkl(data, file):
    with open(file, 'wb') as f:
        pickle.dump(data, f)


def main():
    try:
        logging.info("Extracting data")
        data = csv_file.read_csv(data_path)
    except Exception as e: 
        logger.exception(f"Exception -- Can not read data due to {e}")
        sys.exit(2)
    
    data['pickup_datetime'] = pd.to_datetime(data['pickup_datetime'], format = '%Y-%m-%d %H:%M:%S')
    data['pickup_date'] = data['pickup_datetime'].dt.date
    data['pickup_day'] = data['pickup_datetime'].apply(lambda x : x.day)
    data['pickup_hour'] = data['pickup_datetime'].apply(lambda x : x.hour)
    data['pickup_day_of_week'] = data['pickup_datetime'].apply(lambda x : calendar.day_name[x.weekday()])
    data['pickup_latitude_round3'] = data['pickup_latitude'].apply(lambda x : round(x, 3))
    data['pickup_longitude_round3'] = data['pickup_longitude'].apply(lambda x : round(x, 3))
    data['dropoff_latitude_round3'] = data['dropoff_latitude'].apply(lambda x : round(x, 3))
    data['dropoff_longitude_round3'] = data['pickup_longitude'].apply(lambda x : round(x, 3))

    le = LabelEncoder()
    le.fit(data['store_and_fwd_flag'])
    data['store_and_fwd_flag'] = le.transform(data['store_and_fwd_flag'])

    data['pickup_day_of_week'] = data['pickup_day_of_week'].apply(lambda x:encodeDays(x))
    data["distance"] = data.apply(lambda row: get_distance(row), axis = 1)
    
    coords = np.vstack((data[['pickup_latitude', 'pickup_longitude']].values,
    	               data[['dropoff_latitude', 'dropoff_longitude']].values,
        	           data[['pickup_latitude', 'pickup_longitude']].values,
            	       data[['dropoff_latitude', 'dropoff_longitude']].values))

    kmeans = KMeans(n_clusters = 10, random_state = 0).fit(coords)

    data.loc[:, 'pickup_neighbourhood'] = kmeans.predict(data[['pickup_latitude', 
                                                            'dropoff_longitude']])
    data.loc[:, 'dropoff_neighbourhood'] = kmeans.predict(data[['dropoff_latitude',
                                                            'pickup_longitude']])

    csv_file.write_csv(data, f"s3://{BUCKET}/{INPUT_PREFIX}/preprocess_data-{today}.csv")

    return data

if __name__ == "__main__": 
    main()