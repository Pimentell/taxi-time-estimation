import boto3
import pandas as pd 
import numpy as np
import logging
import sys
from awsglue.utils import getResolvedOptions
import warnings
from utils import csv_file, write_text
import datetime
import json

warnings.filterwarnings("ignore")

# Logger SetUp
# -------------------------------------------------------------------------------
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logFormatter = logging.Formatter(
    "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(logFormatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)


BUCKET = getResolvedOptions(sys.argv, ["bucket"])
BUCKET = BUCKET["bucket"]

INPUT_PREFIX = getResolvedOptions(sys.argv, ["input-prefix"])
INPUT_PREFIX = INPUT_PREFIX["input-prefix"]


METRICS_PREFIX = getResolvedOptions(sys.argv, ["metrics-prefix"])
METRICS_PREFIX = METRICS_PREFIX["metrics-prefix"]


today = str(datetime.datetime.now())[:13].replace(" ", "-").replace("-", "_")

data_path = f"s3://{BUCKET}/{INPUT_PREFIX}/preprocess_data-{today}.csv"

def main(): 
    logging.info("Loading Data")
    data = csv_file.read_csv(data_path)
    metrics = data.describe().to_json()

    logging.info("Saving metrics")
    metrics_path = f"s3://{BUCKET}/{METRICS_PREFIX}/metrics-{today}.json"
    write_text(json.dumps(metrics), metrics_path)

if __name__ == "__main__": 
    main()

