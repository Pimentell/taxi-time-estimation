import numpy as np
import xgboost as xgb
import warnings
import sys
import logging
from sklearn.model_selection import train_test_split
from utils import csv_file, write_text, write_pickle
from awsglue.utils import getResolvedOptions
from sklearn.metrics import mean_squared_error
import datetime
import json

today = str(datetime.datetime.now())[:13].replace(" ", "-").replace("-", "_")


warnings.filterwarnings("ignore")

# Logger SetUp
# -------------------------------------------------------------------------------
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
logFormatter = logging.Formatter(
    "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(logFormatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

BUCKET = getResolvedOptions(sys.argv, ["bucket"])
BUCKET = BUCKET["bucket"]

INPUT_PREFIX = getResolvedOptions(sys.argv, ["input-prefix"])
INPUT_PREFIX = INPUT_PREFIX["input-prefix"]

MODEL_PREFIX = getResolvedOptions(sys.argv, ["model-prefix"])
MODEL_PREFIX = MODEL_PREFIX["model-prefix"]

METRICS_PREFIX = getResolvedOptions(sys.argv, ["metrics-prefix"])
METRICS_PREFIX = METRICS_PREFIX["metrics-prefix"]

data_path = f"s3://{BUCKET}/{INPUT_PREFIX}/preprocess_data-{today}.csv"

def main():
    params = {
    'booster':            'gbtree',
    'objective':          'reg:linear',
    'learning_rate':      0.1,
    'max_depth':          14,
    'subsample':          0.8,
    'colsample_bytree':   0.7,
    'colsample_bylevel':  0.7,
    'silent':             1
    }

    model_columns = [
    "vendor_id", 
    "passenger_count", 
    "pickup_latitude_round3",            
    "pickup_longitude_round3",            
    "dropoff_latitude_round3",          
    "dropoff_longitude_round3",
    "distance", 
    "pickup_neighbourhood", 
    "dropoff_neighbourhood", 
    "store_and_fwd_flag", 
    "pickup_day", 
    "pickup_hour", 
    "pickup_day_of_week"
    ]

    target_column = [
    "trip_duration"
    ]
    try: 
        logging.info("Getting Training data")
        data = csv_file.read_csv(data_path)
        print(data.shape)
    except Exception as e: 
        logger.exception(f"Exception -- Can not read data due to {e}")
        sys.exit(2)

    X_train, X_test, y_train, y_test = train_test_split(data[model_columns], data[target_column], test_size=0.33, random_state=42)
    
    n_rounds = 50

    dtrain = xgb.DMatrix(X_train, np.log(y_train +1))
    gbm = xgb.train(params, dtrain, num_boost_round=n_rounds)

    # Model Metrics

    predict_values  = np.exp(gbm.predict(xgb.DMatrix(X_test[model_columns]))) - 1
    rmse=mean_squared_error(predict_values,y_test)
    logging.info(f"metric: {rmse}")

    logging.info("Saving model Metrics and model")
    metrics_path = f"s3://{BUCKET}/{METRICS_PREFIX}/xgboost-{today}.json"
    write_text(json.dumps({
        "metrics": {
            "rmse":str(rmse)
            }, 
        "model": f"xgboost-{today}"
        }), metrics_path)
    
    model_path = f"s3://{BUCKET}/{MODEL_PREFIX}/xgboost-{today}.pkl"
    write_pickle(xgb, model_path)


if __name__ == "__main__": 
    main()