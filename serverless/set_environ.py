"""
Local Configuration for add an user to aws.config
"""



import boto3
import sys
from pathlib import Path
import os
import sys

TokenCode = sys.argv[1]
SerialNumber = sys.argv[2]
client = boto3.client("sts")
response = client.get_session_token(
    DurationSeconds= 24 * 60 * 60,
    SerialNumber=str(SerialNumber),
    TokenCode=str(TokenCode),
)
new_file = []
flag = False
with open(os.path.join(Path.home(),'.aws/credentials'),'r') as f:
    content = f.read()
    rows = content.split("\n")
    for row in rows:
        if row == "[mfa]":
            flag = True
        elif "[" in row:
            flag = False
            new_file.append(row)
        elif not flag:
            new_file.append(row)

new_file.append("[mfa]")
new_file.append("aws_access_key_id = {}".format(response["Credentials"]["AccessKeyId"]))
new_file.append("aws_secret_access_key = {}".format(response["Credentials"]["SecretAccessKey"]))
new_file.append("aws_session_token = {}".format(response["Credentials"]["SessionToken"]))
new_file.append("region = {}".format("us-west-2"))
raw_content = "\n".join(new_file)

with open(os.path.join(Path.home(),'.aws/credentials'),'w') as f:
    f.write(raw_content)
