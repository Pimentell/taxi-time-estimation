from setuptools import setup

setup(
    name="utils_taxi_trip",
    version="0.1",
    packages=['utils'],
    install_requires=['pandas==1.0.3',
                      'boto3',
                      'botocore<1.20.0,>=1.19.24',
                      'joblib',
                      'numpy==1.18.1',
                      'psycopg2-binary',
                      'pyarrow==0.17.1',
                      'SQLAlchemy==1.3.23',
                      'urllib3!=1.25.0,!=1.25.1,<1.26,>=1.21.1',
                      's3transfer<0.4.0,>=0.3.0'
                      'ipython',
                      's3fs',
                      'unidecode', 
                      'sklearn', 
                      'xgboost'
                      ]
)