import boto3
import pandas as pd
from io import StringIO, BytesIO
import pickle


class csv_file(object):
    __name__ = "CSV Files Class"

    """
    Clase para trabajar con archivos csv desde S3. 

    Methods:

    write_csv
        args: 
        - df: DataFrame
        - path: string path to save 

    read_csv
        args: 
        - path: path to extract
    """

    @classmethod
    def write_csv(cls, df, path: str = None):
        if path[:5] == 's3://':
            s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
            s3_key = '/'.join(s3_key)
            csv_buffer = StringIO()
            df.to_csv(csv_buffer, index=False)
            boto3.client("s3").put_object(Bucket=s3_bucket, Key=s3_key, Body=csv_buffer.getvalue())
            print("Upload Finished")
        else:
            if path is None:
                print("No path received")
            else:
                print("No valid Path. Must be in 's3:\\' format")
        return None

    @classmethod
    def read_csv(cls, path: str = None):
        if path[:5] == 's3://':
            s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
            s3_key = '/'.join(s3_key)
            with BytesIO() as f:
                boto3.client("s3").download_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
                f.seek(0)
                text = f.read().decode()
                table = pd.read_csv(StringIO(text))
        else:
            if path is None:
                print("No path received")
            else:
                print("No valid Path. Must be in 's3:\\' format")
        return table


def write_text(text, path):
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            f.write(text.encode())
            f.seek(0)
            boto3.client("s3").upload_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
    else:
        with open(path, 'wb') as f:
            f.write(text.encode())


def read_text(path):
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            boto3.client("s3").download_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
            f.seek(0)
            text = f.read().decode()
    else:
        with open(path, 'rb') as f:
            text = f.read().decode()
    return text

def write_pickle(obj, path):
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            pickle.dump(obj, f)
            f.seek(0)
            boto3.client("s3").upload_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
    else:
        with open(path, 'wb') as f:
            pickle.dump(obj, f)


def read_pickle(path):
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            boto3.client("s3").download_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
            f.seek(0)
            model = pickle.load(f)
    else:
        with open(path, 'rb') as f:
            model = pickle.load(f)
    return model

