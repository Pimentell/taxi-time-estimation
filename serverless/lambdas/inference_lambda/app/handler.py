import boto3
from .app import predict, parse_requests_data, format_response, firehose_log
import logging
import json


logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)
logger.handlers = []
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.propagate = False


def lambda_handler(event, context): 
    response_content_type = 'application/json'
    try: 
        logging.info("Parsing request")
        invocation_type, response_content_type, json_data = parse_requests_data(event)
        logging.info('Invocation type = {}'.format(invocation_type))
        logging.info("data: ".format(json_data))
    except Exception as e: 
        logging.exception(f"Can not Event Info due to {e}")
        prediction = json.dumps({
            "prediction": "No valid prediction"
        })
        response = format_response(response_content_type, prediction)

    try: 
        logging.info("Get Prediction")
        prediction = json.dumps({
            "prediction": predict(json_data)
            })
    except Exception as e: 
        logging.exception(f"Can not predction due to {e}")
        prediction = json.dumps({
            "prediction": "No valid prediction"
        })
        response = format_response(response_content_type, prediction)
    try: 
        logging.info("Save Prediction into Firehose")
        firehose_log(json_data, prediction)
    except Exception as e: 
        logging.exception(f"Can not predction due to {e}")

    return response
    