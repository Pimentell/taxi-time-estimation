import logging
import boto3
import os
import json
import traceback
import numpy as np
import xgboost as xgb
import pickle
import pandas as pd 


logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)
logger.handlers = []
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.propagate = False


firehose_client = boto3.client("firehose")

def firehose_log(params, response):
    try:
        # Log result to Kinesis Firehose
        firehose_client.put_record(
            DeliveryStreamName=os.environ["model_logs"],
            Record={"Data": json.dumps({"prediction":response["prediction"]}) + "\n"},
        )
    except Exception:
        error_trace = traceback.format_exc()
        logging.warning(json.dumps({"type":"kinesis_firehose_error", "params":params, "trace":error_trace}))

def get_model(model_path): 
    try: 
        with open(model_path, "rb") as model_pickle_file:
            model = pickle.load(model_pickle_file)
    except Exception as e: 
        logging.exception(f"Can not Load Model Due to {e}")
        model = None
    return model

def predict(json_data): 
    logging.info("Performing Prediction")
    try: 
        pred_x_df = pd.DataFrame.from_dict(json_data)
        pred_x = xgb.DMatrix(pred_x_df.values)
        model = get_model()
        predictions = np.exp(model.predict(xgb.DMatrix(pred_x))) - 1
        return predictions
    except Exception as e: 
        logging.exception(f"Can not Get Prediction due to {e}")
        predictions = []
        return predictions

def parse_requests_data(event): 
    response_content_type = 'application/json'
    try:
        pred_x_csv = event['data']
        invocation_type = 'direct_or_api_gateway'
        try:
            response_content_type = event['response_content_type'].strip()
            if response_content_type is None:
                response_content_type = 'application/json'
            elif len(response_content_type) == 0:
                response_content_type = 'application/json'
            elif response_content_type != 'text/plain':
                response_content_type = 'application/json'
        except KeyError:
            pass
    except KeyError:
        invocation_type = 'api_gateway'
        json_data = json.loads(event['body'])['data']
    return invocation_type, response_content_type, json_data
        
def format_response(response_content_type, return_raw_data): 
    logging.info('Formatting response data...')
    if response_content_type == 'text/plain':
        return_data = return_raw_data
    else:
        return_data = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.dumps({
                "Predicted value": return_raw_data
            })
        }
    return return_data