import boto3
import json
import logging
import json
import datetime
import os 

logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)
logger.handlers = []
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.propagate = False

today = str(datetime.datetime.now())[:13].replace(" ", "-").replace("-", "_")

bucket = os.environ["BUCKET"]
prefix = os.environ["PREFIX"]
sns_topic = os.environ["SNS_TOPIC"]

sns_client = boto3.client("sns")

def lambda_handler(event, context):
    s3_resource = boto3.resource("s3")
    content_object = s3_resource.Object(bucket, f"{prefix}/xgboost-{today}.json")
    file_content = content_object.get()['Body'].read().decode('utf-8')
    json_content = json.loads(file_content)
    if float(json_content["metrics"]["rsme"]) > 9776381.642750546: 
        response = sns_client.publish(
        TopicArn=sns_topic,
        Message= json.dumps({
            "Metrics": str(json_content["metrics"]["rsme"])
        }),
        Subject="Training Metrics for Taxi-trip",
    )
    return response
    
