# Serverless Version

It allows to use in a much more flexible way the training capabilities in Glue Job and Lambda. Using as the main instrument for the orchestration the creation of pipelines through StepFunctions.

One of the main advantages is the permissibility that it gives the ML engineer to be able to modify any of the characteristics within the processing or even the training steps As well as adding different monitoring components, alarms and even automations

![Imagen](images/ServerlessMLTaxi.jpeg)