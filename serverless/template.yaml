AWSTemplateFormatVersion: '2010-09-09'

Transform: AWS::Serverless-2016-10-31


Globals:
  Function:
    Runtime: python3.7
    MemorySize: 1024
    Timeout: 360


Parameters: 
  Bucket:
    Type: String
    Description: "Bucket to Store Data"
    Default: "datalake-model-artifacts"
  ProjectName: 
    Type: String
    Description: "Project Name"
    Default: "taxi-trip-model"
  Version: 
    Type: String
    Description: "CLoudformation Stack Version"
    Default: "v1"
  OwnerEmail: 
    Type: String
    Description: "Owner email"
    Default: "vpimentel0613@hotmail.com"


Resources:
  ###########
  ## Lambda API Services
  ###########
  TaxiTripServerlessApi:
    Type: AWS::Serverless::HttpApi
    Properties:
      StageName: Prod
      CorsConfiguration:
        AllowMethods:
          - "*"
        AllowHeaders:
          - "*"
        AllowOrigins:
          - "*"

  TaxiTripApi:
    Type: AWS::Serverless::Function
    Properties:
      Timeout: 180
      MemorySize: 2048
      FunctionName: !Sub '${Project}-${Stage}-${Version}-LambdaInference'
      PackageType: Image
      Events:
        HelloWorld:
          Type: HttpApi
          Properties:
            ApiId: !Ref TaxiTripServerlessApi
            Path: /predict
            Method: post
    Metadata:
      Dockerfile: Dockerfile
      DockerContext: ./
      DockerTag: !Sub '${TagId}'

  ############
  ## Glue Services
  ############
  GlueAccessPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Sub "${ProjectName}-${Version}-${Stage}-GluePolicy"
      Roles:
        - !Ref GlueRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action: 's3:*'
            Resource:
              - !Sub "arn:aws:s3:::${Bucket}"
  GlueRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: !Sub "${ProjectName}-${Version}-${Stage}-GlueRole"
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: 'Allow'
            Principal:
              Service:
                - 'glue.amazonaws.com'
            Action:
              - 'sts:AssumeRole'

  DataValidation:
    Type: AWS::Glue::Job
    Properties:
      Name: !Sub "${ProjectName}-${Version}-${Stage}-GlueDataValidation"
      MaxCapacity: 1
      Command:
        Name: pythonshell
        PythonVersion: 3
        ScriptLocation: glueJobs/data_validation.py
      DefaultArguments:
        '--bucket': !Ref Bucket
        '--input-prefix': !Sub "${ProjectName}/${Stage}/preprocess/output"
        '--metrics-prefix': !Sub "${ProjectName}/${Stage}/preprocess/metrics"
        '--extra-files': !Sub "s3://${Bucket}/${ProjectName}/${Stage}/utils/utils_taxi_trip-1.0-py3-none-any.whl"
      Description: "Data Preprocess and validate drift"
      Role: !GetAtt GlueRole.Arn

  Preprocess:
    Type: AWS::Glue::Job
    Properties:
      Name: !Sub "${ProjectName}-${Version}-${Stage}-GluePreprocess"
      MaxCapacity: 1
      Command:
        Name: pythonshell
        PythonVersion: 3
        ScriptLocation: glueJobs/preprocess.py
      DefaultArguments:
        '--bucket': !Ref Bucket
        '--input-prefix': !Sub "${ProjectName}/raw_data"
        '--output-prefix': !Sub "${ProjectName}/${Stage}/preprocess/metrics"
        '--extra-files': !Sub "s3://${Bucket}/${ProjectName}/${Stage}/utils/utils_taxi_trip-1.0-py3-none-any.whl"
      Description: "Data Preprocess and validate drift"
      Role: !GetAtt GlueRole.Arn

  Training: 
    Type: AWS::Glue::Job
    Properties:
      Name: !Sub "${ProjectName}-${Version}-${Stage}-GlueTrain"
      MaxCapacity: 1
      Command:
        Name: pythonshell
        PythonVersion: 3
        ScriptLocation: gluejobs/training.py
      DefaultArguments:
        '--bucket': !Ref Bucket
        '--input-prefix': !Sub "${ProjectName}/${Stage}/preprocess/output"
        '--model-prefix': !Sub "${ProjectName}/${Stage}/training/model"
        '--metrics-prefix': !Sub "${ProjectName}/${Stage}/training/metrics"
        '--extra-files': !Sub "s3://${Bucket}/${ProjectName}/${Stage}/utils/utils_taxi_trip-1.0-py3-none-any.whl"
      Description: 'Train model'
      Role: !GetAtt GlueRole.Arn


  #############
  ## Firehose Services
  ############# 

  DeliveryStreamPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Sub "${ProjectName}-${Version}-${Stage}-DeliveryStreamPolicy"
      Roles:
        - !Ref DeliveryStreamRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - 's3:AbortMultipartUpload'
              - 's3:GetBucketLocation'
              - 's3:GetObject'
              - 's3:ListBucket'
              - 's3:ListBucketMultipartUploads'
              - 's3:PutObject'
            Resource:
              - !Sub "arn:aws:s3:::${Bucket}"

  DeliveryStreamRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub "${ProjectName}-${Version}-${Stage}-DeliveryStreamRole"
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: ''
            Effect: Allow
            Principal:
              Service: firehose.amazonaws.com
            Action: 'sts:AssumeRole'
            Condition:
              StringEquals:
                'sts:ExternalId': !Ref 'AWS::AccountId'

  DeliveryStream:
    Type: AWS::KinesisFirehose::DeliveryStream
    DependsOn:
      - DeliveryStreamPolicy
    Properties:
      DeliveryStreamName: !Sub "${ProjectName}-${Version}-${Stage}-DeliveryStream"
      DeliveryStreamType: DirectPut
      ExtendedS3DestinationConfiguration:
        Prefix: !Sub '${ProjectName}/${Stage}/inference/predictions'
        BucketARN: !Sub 'arn:aws:s3:::${Bucket}'
        BufferingHints:
          IntervalInSeconds: 360
          SizeInMBs: 10
        CompressionFormat: GZIP
        RoleARN: !GetAtt DeliveryStreamRole.Arn

  #############
  ## Alarm Services for Metrics in Trainin glue
  ############

  TrainingTopic:
    Type: AWS::SNS::Topic
    Properties: 
      TopicName: !Sub '${Project}-${Stage}-${Version}-TrainTopic'

  TrainingTopicSubscription:
    Type: AWS::SNS::Subscription
    Properties:
      Endpoint: !Ref OwnerEmail
      Protocol: email
      TopicArn: !Ref TrainingTopic

  LambdaValidation:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: !Sub '${Project}-${Stage}-${Version}-LambdaInference'
      Timeout: 180
      MemorySize: 2048
      CodeUri: lambdas/validation_lambda/
      Handler: app.app.lambda_handler
      Environment:
        Variables:
          BUCKET: !Ref Bucket
          PREFIX: "${ProjectName}/${Stage}/training/metrics"
          SNS_TOPIC: !Ref TrainingTopic
      Policies:
        - Version: '2012-10-17' # Policy Document
          Statement:
            - Effect: Allow
              Action:
                - sns:*
              Resource:
                - !Ref TrainingTopic



  #############
  ## Alarm Services for pipeline
  ############

  ModelSubscription:
    Type: AWS::SNS::Topic

  EmailSubscription:
    Type: AWS::SNS::Subscription
    Properties:
      Endpoint: !Ref OwnerEmail
      Protocol: email
      TopicArn: !Ref ModelSubscription
  
  RetrainFail:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub "${ProjectName}-${Version}-${Stage}-RetrainFail"
      AlarmActions:
        - !Ref ModelSubscription
      ComparisonOperator: GreaterThanOrEqualToThreshold
      EvaluationPeriods: 1
      MetricName: "ExecutionsFailed"
      Namespace: "AWS/States"
      Dimensions:
      - Name: StateMachineArn
        Value: !Ref Pipeline
      Period: 360
      Statistic: Sum
      Threshold: "1"

  RetrainSucceed:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub "${ProjectName}-${Version}-${Stage}-RetrainSucceed"
      AlarmActions:
        - !Ref ModelSubscription
      ComparisonOperator: GreaterThanOrEqualToThreshold
      EvaluationPeriods: 1
      MetricName: "ExecutionsSucceeded"
      Namespace: "AWS/States"
      Dimensions:
      - Name: StateMachineArn
        Value: !Ref Pipeline
      Period: 360
      Statistic: Sum
      Threshold: '1'


  ##############
  ## Metric Services for lambda Service
  ##############
  ErrorMetric:
    Type: AWS::Logs::MetricFilter
    Properties:
      LogGroupName: !Sub '/aws/lambda/${TaxiTripApi}'
      FilterPattern: '{$.type = "alarm"}'
      MetricTransformations:
        -
          MetricValue: "1"
          MetricNamespace: !Sub "${ProjectName}-${Version}-${Stage}"
          MetricName: "ErrorAlarm"


  Pipeline:
    Type: AWS::Serverless::StateMachine
    Properties:
      Name: !Sub "${ProjectName}-${Version}-${Stage}-StateMachine"
      Policies:
        - CloudWatchPutMetricPolicy: {}
        - Version: '2012-10-17' # Policy Document
          Statement:
            - Effect: Allow
              Action:
                - glue:*
              Resource:
                - !Sub 'arn:${AWS::Partition}:glue:${AWS::Region}:${AWS::AccountId}:job/${DataValidation}'
                - !Sub 'arn:${AWS::Partition}:glue:${AWS::Region}:${AWS::AccountId}:job/${Preprocess}'
                - !Sub 'arn:${AWS::Partition}:glue:${AWS::Region}:${AWS::AccountId}:job/${Training}'
            - Effect: Allow
              Action: 
                - lambda:*
              Resource: 
                - !GetAtt LambdaValidation.Arn
      DefinitionUri: step_function.json
      DefinitionSubstitutions:
        PreprocessName: !Ref Preprocess
        DataValidationName: !Ref DataValidation
        TrainingName: !Ref Training
        LambdaValidationName: !GetAtt LambdaValidation.Arn
      Events:
        RetrainTrigger:
          Type: Schedule
          Properties:
            Description: Schedule to retrain model bi weekly
            Enabled: False
            Schedule: "cron(0 0 1/15 * ? *)"

Outputs:
  Pipeline:
    Description: 'Step Function pipeline for running Data Ingestion and Model Retraining'
    Value: !Ref Pipeline
